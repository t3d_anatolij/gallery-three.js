import {
    Vector3,
    Raycaster,
    Vector2,
    CylinderGeometry,
    MeshBasicMaterial,
	Mesh,
	SpriteMaterial,
	Sprite,
	TextureLoader
  } from 'three';
import { PointerLockControls } from 'three/examples/jsm/controls/PointerLockControls.js';
import TWEEN from '@tweenjs/tween.js';

import {config} from './../config';
import {configrus} from './../configrus';

import SpriteTexture from '../../assets/point.png';
const painting = require.context('./../../assets/paintings', true, /\.jpg$/)

const YTPlayer = require('yt-player')

export default class PointerLock {
    constructor(viewer) {
        this.viewer = viewer;
        this.moveForward = false;
		this.moveBackward = false;
		this.moveLeft = false;
		this.moveRight = false;
		this.velocity = new Vector3();
		this.prevTime = performance.now();
		this.direction = new Vector3();
		this.previousStep = new Vector3(0, 10 , 0);
		this.mouse = new Vector2(0, 0);
		this.frontRay = new Raycaster();
		this.point = {};
		this.spriteMap = new TextureLoader().load( SpriteTexture );
		this.paintings = painting.keys()
			.reduce((images, key) => {
			images[key] = painting(key); 
			return images
			}, {})
		this.player = new YTPlayer('#ytplayer')
        this.init();
    }
  
    init() {

        var geometry = new CylinderGeometry( 0.3, 0.3, 0.02, 32 );
		let material = new MeshBasicMaterial( {color: 0xffffff, transparent: true, opacity : 0} );
		this.circle = new Mesh( geometry, material );
        this.viewer.scene.add( this.circle );
        

        this.controls = new PointerLockControls( this.viewer.camera, this.viewer.renderer.domElement );
		window.controls = this.controls;
		this.viewer.container.addEventListener( 'click',  () => {
			this.controls.lock();
			if(this.painting){
				this.controls.unlock();
			}
			

		}, false );
		document.getElementById('hero__virtual-tour').addEventListener( 'click',  () => {
			this.controls.lock();
			
		}, false );
		

		document.getElementById('painting__close').addEventListener( 'click',  () => {
			this.controls.lock();
			this.player.stop()
			
		}, false );
		

		

		this.viewer.scene.add( this.controls.getObject() );
		var onKeyDown =  ( event ) => {

			switch ( event.keyCode ) {

				case 38: // up
				case 87: // w
					this.moveForward = true;
					break;

				case 37: // left
				case 65: // a
					this.moveLeft = true;
					break;

				case 40: // down
				case 83: // s
					this.moveBackward = true;
					break;

				case 39: // right
				case 68: // d
					this.moveRight = true;
					break;

			}

		};

		var onKeyUp =  ( event ) => {

			switch ( event.keyCode ) {

				case 38: // up
				case 87: // w
					this.moveForward = false;
					break;

				case 37: // left
				case 65: // a
					this.moveLeft = false;
					break;

				case 40: // down
				case 83: // s
					this.moveBackward = false;
					break;

				case 39: // right
				case 68: // d
					this.moveRight = false;
					break;

			}

		};

		document.addEventListener( 'keydown', onKeyDown, false );
		document.addEventListener( 'keyup', onKeyUp, false );

        this.raycaster = new Raycaster( new Vector3(), new Vector3( 0, - 1, 0 ), 0, 100 );
        this.viewer.container.addEventListener('click', this.onClick.bind(this));
    }

    motion(){
        if(this.controls.isLocked){
			this.portal();
			this.raycaster.ray.origin.copy( this.controls.getObject().position );
			let intersections = this.raycaster.intersectObjects( this.viewer.meshOnScene );
            let onObject = intersections.length > 0 && intersections[0].object.name === "collission";
			let time = performance.now();
			let delta = ( time - this.prevTime ) / 1000;

			this.velocity.x -= this.velocity.x * 10.0 * delta;
			this.velocity.z -= this.velocity.z * 10.0 * delta;

			this.velocity.y -= 9.8 * 100.0 * delta; // 100.0 = mass

			this.direction.z = Number( this.moveForward ) - Number( this.moveBackward );
			this.direction.x = Number( this.moveRight ) - Number( this.moveLeft );
			this.direction.normalize(); 

			if ( this.moveForward || this.moveBackward ) this.velocity.z -= this.direction.z * 20.0 * delta;
			if ( this.moveLeft || this.moveRight ) this.velocity.x -= this.direction.x * 20.0 * delta;

			if(onObject){ //onObject
				this.previousStep.copy( this.controls.getObject().position );
			}else{
				this.controls.getObject().position.copy(this.previousStep);
			}

			this.controls.moveRight( - this.velocity.x * delta );
			this.controls.moveForward( - this.velocity.z * delta );

			this.controls.getObject().position.y += ( this.velocity.y * delta ); // new behavior

			if ( this.controls.getObject().position.y < 1.7 ) {

				this.velocity.y = 0;
				this.controls.getObject().position.y = 1.7;

			}

            this.prevTime = time;
		}else{
			this.velocity.x = 0;
			this.velocity.z = 0;
		}
    }

    portal(){
		if(this.controls.isLocked){ 

			this.frontRay.setFromCamera(this.mouse, this.viewer.camera);
            this.intersects = this.frontRay.intersectObjects(this.viewer.meshOnScene);
			if(this.intersects.length > 0 && this.intersects[0].object.name === "collission"){
                this.circle.material.opacity = 0.4;
				this.circle.position.copy(this.intersects[0].point);			
			}else{
				this.circle.material.opacity = 0;
			}

			
			if(this.intersects.length > 0 && 
				(config[this.intersects[0].object.name] || configrus[this.intersects[0].object.name] ) && 
				this.intersects[0].distance < 2
			){
				
				if(!this.point[this.intersects[0].object.name]){

					const spriteMaterial = new SpriteMaterial( { map: this.spriteMap } );
					const sprite = new Sprite( spriteMaterial );
					sprite.position.copy(this.intersects[0].object.position);
					sprite.position.x += -5;
					sprite.position.z += -6;
					sprite.scale.set(0.1, 0.1, 0.1);
					this.viewer.scene.add( sprite );
					this.viewer.scene.add( sprite );

					let side = this.intersects[0].face.normal.applyEuler(this.intersects[0].object.rotation)
					if(side.x < -0.9){
						sprite.position.x -= 0.1;
					}else if(side.x > 0.9){
						sprite.position.x += 0.1;
					}else if(side.z > 0.9){
						sprite.position.z += 0.1;
					}else if(side.z < -0.9){
						sprite.position.z -= 0.1;
					}
					
					this.intersects[0].object.geometry.computeBoundingBox();

					let size = {
						x : this.intersects[0].object.geometry.boundingBox.max.x - this.intersects[0].object.geometry.boundingBox.min.x,
						y : this.intersects[0].object.geometry.boundingBox.max.y - this.intersects[0].object.geometry.boundingBox.min.y,
						z : this.intersects[0].object.geometry.boundingBox.max.z - this.intersects[0].object.geometry.boundingBox.min.z
					}
					
					new TWEEN.Tween({scale : 0.665}) 
						.to({scale : 1.5}, 1000)
						.easing(TWEEN.Easing.Quartic.InOut)
						.repeat(Infinity)
						.onUpdate((value)=> { 
							if( value.scale < 1){
								sprite.scale.set(0.1 / value.scale, 0.1 / value.scale, 0.1 / value.scale);
							}else{
								sprite.scale.set(0.1 * value.scale, 0.1 * value.scale, 0.1 * value.scale);
							}
							

						})
						.start();


					sprite.position.y -= size.y / 2;
					
					
					sprite.name = this.intersects[0].object.name;
					if(this.curent)
						this.curent.visible = false;
					this.curent = sprite;
					this.point[this.intersects[0].object.name] = sprite;


				}else{
					if(this.curent && this.curent.name !== this.point[this.intersects[0].object.name].name)
						this.curent.visible = false;
					this.curent = this.point[this.intersects[0].object.name];
					this.point[this.intersects[0].object.name].visible = true;
				}
				this.painting = this.intersects[0];
				if(this.intersects[0].object.name === 'horse' || 
				this.intersects[0].object.name ===  'Cube.001_0' ||
				this.intersects[0].object.name ===  'Cube.001_1' 
				){
					document.getElementById('select-point').classList.remove('none');
				}else{
					document.getElementById('select-point').classList.add('none');
				}
			}else{
				if(this.painting && this.point[this.painting.object.name]){
					this.point[this.painting.object.name].visible = false;
				}
				document.getElementById('select-point').classList.add('none');
				this.painting = null;
			}
		}
    }
    
    onClick(){
        
		if(this.controls.isLocked && this.intersects.length > 0 && this.intersects[0].object.name === "collission"){

			let coords = { x: this.controls.getObject().position.x, z: this.controls.getObject().position.z };
			new TWEEN.Tween(coords) 
				.to({ x: this.intersects[0].point.x, z: this.intersects[0].point.z }, 1000) 
				.easing(TWEEN.Easing.Linear.None)
				.onUpdate(()=> { 
					this.controls.getObject().position.x = coords.x;
					this.controls.getObject().position.z = coords.z;

				})
				.start();
			
		}
		

		if(this.painting){
			
			if(document.getElementById('select-language').value === "English"){
				this.data = config[this.painting.object.name];
			}else{
				this.data = configrus[this.painting.object.name];
			}
			
			document.getElementById('popup-painting').classList.add('active');
			document.getElementById('painting__title').innerHTML = this.data.title;


			let temp0 = '';
			this.data.description.forEach(item => {
				temp0 += "<p>" + item + "</p>";
			});
			document.getElementById('painting__desc-text').innerHTML = temp0;

			let temp = '';
			this.data.details.forEach(item => {
				temp += "<li class=\"painting__desc-list-item\">" + item + "</li>";
			});
			document.getElementById('details').innerHTML = temp;

			document.getElementById('painting__img').style.backgroundImage = "url("+this.paintings[this.data.painting]+")"
			document.getElementById('painting__mobile').src = this.paintings[this.data.smallPainting];

			if(this.data.video){
				document.getElementById('painting__controls-video').style.display = 'block';
				this.player.load(this.data.video)
			} else{
				document.getElementById('painting__controls-video').style.display = 'none';
			}
			

			let temp1 = '';
			this.data.mainText.forEach(item => {
				temp1 += "<p>" + item + "</p><br>";
			});
			document.getElementById('main-text').innerHTML = temp1;

			let temp2 = '';
			this.data.originalText.forEach(item => {
				temp2 += "<p>" + item + "</p><br>";
			});
			document.getElementById('original-text').innerHTML = temp2;

			let temp3 = '';
			this.data.translatedText.forEach(item => {
				temp3 += "<p>" + item + "</p><br>";
			});
			document.getElementById('translated-text').innerHTML = temp3;

			
			
		}
		
	}
}