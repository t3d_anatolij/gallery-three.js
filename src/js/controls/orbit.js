import {
    Vector3,
    Raycaster,
    Vector2,
	TextureLoader
  } from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import TWEEN from '@tweenjs/tween.js';

import {config} from './../config';
import {configrus} from './../configrus';
import SpriteTexture from '../../assets/point.png';

const painting = require.context('./../../assets/paintings', true, /\.jpg$/)
const YTPlayer = require('yt-player')

export default class Orbit {
    constructor(viewer) {
        this.viewer = viewer;
        this.mouse = new Vector2();
        this.frontRay = new Raycaster();
        this.spriteMap = new TextureLoader().load( SpriteTexture );
        this.player = new YTPlayer('#ytplayer')
        this.paintings = painting.keys()
			.reduce((images, key) => {
			images[key] = painting(key); 
			return images
			}, {})
        this.init();
    }
  
    init() {
        this.orbit = new OrbitControls(this.viewer.camera, this.viewer.renderer.domElement);
        this.orbit.target = new Vector3 (0, 1.7, 0);
        this.orbit.rotateSpeed = 0.6;
        // this.orbit.addEventListener('change', ()=> { 
        //     this.viewer.interactive.interactivePoint();
        // })
        this.viewer.container.addEventListener('touchstart', (event)=>{
            this.time = new Date().getTime();
            this.changedTouches = {
                x: event.changedTouches[0].clientX,
                y: event.changedTouches[0].clientY
            }
            
        });
        this.viewer.container.addEventListener('touchend', this.onTouch.bind(this));
        document.getElementById('painting__close').addEventListener( 'touchend',  () => {
			this.player.stop()
			
        }, false );
        this.orbit.update();
    }

    onTouch(event){
		this.mouse.x = +(event.changedTouches[0].pageX / this.viewer.container.offsetWidth) * 2 +-1;
		this.mouse.y = -(event.changedTouches[0].pageY / this.viewer.container.offsetHeight) * 2 + 1;


		this.frontRay.setFromCamera(this.mouse, this.viewer.camera);
		this.intersects = this.frontRay.intersectObjects(this.viewer.meshOnScene);
		
		let temp1 = Math.abs(this.changedTouches.x - event.changedTouches[0].clientX);
		let temp2 = Math.abs(this.changedTouches.y - event.changedTouches[0].clientY);
		if(	new Date().getTime() - this.time < 400 &&  
            this.intersects.length > 0 &&
			temp1 < 5 &&
			temp2 < 5
		){

			if(this.intersects[0].object.name === "collission"){
                const diff = this.viewer.camera.position.clone().sub(this.orbit.target.clone());
                const newTarget = this.intersects[0].point.clone();
                const newPosition = newTarget.clone().add(diff);
                newTarget.y = 1.7;
                newPosition.y = 1.7 + diff.y;
        
                this.flyTo(newPosition, newTarget);
            }else if(config[this.intersects[0].object.name] ){

                if(document.getElementById('select-language-mobile').value === "En"){
                    this.data = config[this.intersects[0].object.name];
                }else{
                    this.data = configrus[this.intersects[0].object.name];
                }
                document.getElementById('popup-painting').classList.add('active');
                document.getElementById('painting__title').innerHTML = this.data.title;


                let temp0 = '';
                this.data.description.forEach(item => {
                    temp0 += "<p>" + item + "</p>";
                });
                document.getElementById('painting__desc-text').innerHTML = temp0;

                let temp = '';
                this.data.details.forEach(item => {
                    temp += "<li class=\"painting__desc-list-item\">" + item + "</li>";
                });
                document.getElementById('details').innerHTML = temp;

                document.getElementById('painting__img').style.backgroundImage = "url("+this.paintings[this.data.painting]+")"
                document.getElementById('painting__mobile').src = this.paintings[this.data.smallPainting];

                if(this.data.video){
                    document.getElementById('painting__controls-video').style.display = 'block';
                    this.player.load(this.data.video)
                } else{
                    document.getElementById('painting__controls-video').style.display = 'none';
                }
                

                let temp1 = '';
                this.data.mainText.forEach(item => {
                    temp1 += "<p>" + item + "</p><br>";
                });
                document.getElementById('main-text').innerHTML = temp1;

                let temp2 = '';
                this.data.originalText.forEach(item => {
                    temp2 += "<p>" + item + "</p><br>";
                });
                document.getElementById('original-text').innerHTML = temp2;

                let temp3 = '';
                this.data.translatedText.forEach(item => {
                    temp3 += "<p>" + item + "</p><br>";
                });
                document.getElementById('translated-text').innerHTML = temp3;
                }
			
		}
    }
    

	flyTo(newPosition, newTarget) {

		const oldPos = this.viewer.camera.position.clone();
        const oldTarget = this.orbit.target.clone();

        this.orbit.enabled = false;
        new TWEEN.Tween({alpha: 0})
            .to({alpha: 1}, 1000)
            .easing(TWEEN.Easing.Linear.None)
            .onUpdate((delta) => {

                this.viewer.camera.position.copy(oldPos.clone().lerp(newPosition.clone(), delta.alpha));
                this.orbit.target.copy(oldTarget.clone().lerp(newTarget.clone(), delta.alpha));

            })
            .onComplete(() => {
                this.orbit.enabled = true;
            })
            .start();
	}
}