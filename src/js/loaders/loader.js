import HouseModel from './../../assets/model/mus_v_1.4.glb';

import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

import {
    TextureLoader,
    MeshBasicMaterial,
    DoubleSide
  } from 'three';

import TWEEN from '@tweenjs/tween.js';

import SpriteTexture from '../../assets/point.png';
const svgs = require.context('./../../assets/Lightmaps_jpg', true, /\.jpg$/)

export default class Loader {
    constructor(viewer) {
      this.viewer = viewer;
      this.loader = new GLTFLoader();
      this.loaderTexture = new TextureLoader();
      this.time = new Date().getTime();
      this.counter = 0;
      this.spriteMap = new TextureLoader().load( SpriteTexture );
      this.material = new MeshBasicMaterial( {transparent: true, map : this.spriteMap, side : DoubleSide} );

      this.mapName = svgs.keys()
        .reduce((images, key) => {
        images[key] = svgs(key); 
        return images
        }, {})
      
    }

    loadModel() {

        this.loader.load(
            HouseModel,
             ( gltf ) => {
        
                gltf.scene.position.set(-5, 0, -6) //-5
                this.viewer.scene.add( gltf.scene );
                const model = gltf.scene;

                model.traverse(el => {
                    if (el.isMesh) {
                        el.material.roughness = 1;
                        this.loadLightMap(el);
                        
                        if(el.name === "collission"){
                            el.material.transparent = true;
                            el.material.opacity = 0; 
                        }
                        
                
                        if(el.name.includes('point_')){
                            el.material = this.material;
                            el.material.needsUpdate = true;

                            this.viewer.interactivePoints.push(el);
                            
                            el.userData.anim = new TWEEN.Tween({scale : 0.665}) 
                                .to({scale : 1.5}, 1000)
                                .easing(TWEEN.Easing.Quartic.InOut)
                                .repeat(Infinity)
                                .onUpdate((value)=> { 
                                    if( value.scale < 1){
                                        el.scale.set(1 / value.scale, 1 / value.scale, 1 / value.scale);
                                    }else{
                                        el.scale.set(1 * value.scale, 1 * value.scale, 1 * value.scale);
                                    }
                                    

                                })
                            

                            el.visible = false;

                        }else{
                            if(!el.name.includes('Plane_'))
                                this.viewer.meshOnScene.push(el);
                        }
                    }
                });
                
            }
        );
        
    }
    loadLightMap(el){
        
        if(this.mapName[`./${el.name}.jpg`]){

            this.counter ++ ;   
                        
            this.loaderTexture.load(

                this.mapName[`./${el.name}.jpg`],

                ( texture ) => {

                    el.material.lightMap = texture;
                    el.material.needsUpdate = true;

                    this.counter -- ;
                    if(this.counter <= 0){
                        let timerId = setInterval(() => {
                            if(new Date().getTime() - this.time > 12000){
                                document.getElementById('loader').remove();
                                clearInterval(timerId);
                            }   
                        }, 500);
                    }

                },   

                undefined,
            
                function ( err ) {
                    console.error( 'An error happened.' );
                }
            );
        }

    }
}