# gallery-three.js

## Getting Started

#### Example:  

Clone down this repository. You will need `node` and `npm` installed globally on your machine.  

Installation:

`npm install`   

To Start Server:

`npm start`  

To Visit App:

`localhost:3000`  

## Getting Build

#### Example:  

Clone down this repository. You will need `node` and `npm` installed globally on your machine.  

To build:

`npm run build`  

To Visit directory `build` in root project. 
